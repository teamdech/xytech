package dechmods.xytech.items;

import java.util.List;

import dechmods.xytech.XyTech;

import net.minecraft.client.renderer.texture.IconRegister;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.Icon;

public class ItemXychorium extends Item
{
    public Icon[] icons = new Icon[5];
    public String type;
    
    public ItemXychorium(int itemID, String type)
    {
        super(itemID);
        this.type = type;
        setHasSubtypes(true).setCreativeTab(XyTech.creativeTab);
    }
    
    @Override
    public Icon getIconFromDamage(int dmg)
    {
        return dmg >= 0 && dmg < 5 ? icons[dmg] : icons[0];
    }
    
    @Override
    public String getUnlocalizedName(ItemStack stack)
    {
        return getUnlocalizedName() + "." + stack.getItemDamage();
    }
    
    @Override
    public void getSubItems(int itemID, CreativeTabs tab, List list)
    {
        for (int i = 0; i < icons.length; i++)
            list.add(new ItemStack(itemID, 1, i));
    }
    
    @Override
    public void registerIcons(IconRegister iconReg)
    {
        for (int i = 0; i < XyTech.colors.length; i++)
            icons[i] = iconReg.registerIcon("xytech:" + type + XyTech.colors[i]);
    }
}
