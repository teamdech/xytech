package dechmods.xytech.items;

import cpw.mods.fml.common.registry.GameRegistry;
import dechmods.xytech.util.Config;
import net.minecraft.item.Item;

public class Items {
    
    public static Item itemXychorium, itemXychoridite;
    
    public static void initItems()
    {
        itemXychorium = new ItemXychorium(Config.itemXychoriumID, "itemXychorium").setUnlocalizedName("itemXychorium");
        itemXychoridite = new ItemXychorium(Config.itemXychoriditeID, "ingotXychoridite").setUnlocalizedName("itemXychoridite");

        GameRegistry.registerItem(itemXychorium, "itemXychorium");
        GameRegistry.registerItem(itemXychoridite, "itemXychoridite");
    }
}
