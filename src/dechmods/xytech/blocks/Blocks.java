package dechmods.xytech.blocks;

import cpw.mods.fml.common.registry.GameRegistry;
import dechmods.xytech.util.Config;
import net.minecraft.block.Block;

public class Blocks
{
    public static Block blockOres, blockStorage, blockTiles, blockBricks;
    
    public static void initBlocks()
    {
        blockOres = new BlockOres(Config.blockOresID);
        GameRegistry.registerBlock(blockOres, ItemXyBlock.class, "blockOres");
        blockStorage = new BlockDecoration(Config.blockStorageID, "blockXyStorage", "frame/frameFull", "deco/storage");
        GameRegistry.registerBlock(blockStorage, ItemXyBlock.class, "blockStorage");
        blockTiles = new BlockDecoration(Config.blockTilesID, "blockXyTiles", "frame/frameQuarters", "deco/tiles");
        GameRegistry.registerBlock(blockTiles, ItemXyBlock.class, "blockTiles");
        blockBricks = new BlockDecoration(Config.blockBricksID, "blockXyBricks", "frame/frameBricks", "deco/bricks");
        GameRegistry.registerBlock(blockBricks, ItemXyBlock.class, "blockBricks");
    }
}
