package dechmods.xytech.blocks;

import java.util.List;

import dechmods.xytech.XyTech;
import dechmods.xytech.render.MultiLayerRenderer;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.client.renderer.texture.IconRegister;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.ItemStack;
import net.minecraft.util.Icon;
import net.minecraft.world.IBlockAccess;

public class BlockDecoration extends Block implements IMultiLayerRendering
{
    public Icon texture;
    public Icon[] opaque = new Icon[5];
    public static Icon[] fluids;
    
    public static final String[] names = new String[]
    { "Red", "Green", "Blue", "White", "Dark" };
    
    public String textureName, opaqueName;
    
    public BlockDecoration(int blockID, String name, String textureName, String opaqueName)
    {
        super(blockID, Material.iron);
        setHardness(3F).setResistance(2F).setCreativeTab(XyTech.creativeTab).setUnlocalizedName(name);
        
        this.textureName = textureName;
        this.opaqueName = opaqueName;
    }
    
    @Override
    public Icon getIcon(int side, int meta)
    {
        return opaque[meta];
    }
    
    @Override
    public void registerIcons(IconRegister iconReg)
    {
        texture = iconReg.registerIcon("xytech:" + textureName);
        
        for (int i = 0 ; i < opaque.length; i++)
            opaque[i] = iconReg.registerIcon("xytech:" + opaqueName + names[i]);
        
        if (fluids == null)
        {
            fluids = new Icon[names.length];
            for (int i = 0; i < names.length; i++)
                fluids[i] = iconReg.registerIcon("xytech:fluid/fluid" + names[i]);
        }
    }

    @Override
    public Icon getWorldTextureForPass(IBlockAccess world, int x, int y, int z, int side, int pass)
    {
        int meta = world.getBlockMetadata(x, y, z);
        
        if (pass == 0) return texture;
        else return fluids[meta];
    }
    
    @Override
    public int damageDropped(int meta)
    {
        return meta;
    }
    
    @Override
    public void getSubBlocks(int blockID, CreativeTabs tab, List list)
    {
        for (int i = 0; i < 5; i++)
            list.add(new ItemStack(blockID, 1, i));
    }
    
    @Override
    public int getRenderType()
    {
        return MultiLayerRenderer.renderID;
    }
}
