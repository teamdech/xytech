package dechmods.xytech.blocks;

import java.util.List;
import java.util.Random;

import dechmods.xytech.XyTech;
import dechmods.xytech.items.Items;
import dechmods.xytech.render.MultiLayerRenderer;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.client.renderer.texture.IconRegister;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.ItemStack;
import net.minecraft.util.Icon;
import net.minecraft.world.IBlockAccess;

public class BlockOres extends Block implements IMultiLayerRendering
{
    public static Icon[] textures = new Icon[15];
    
    public static final String[] names = new String[]
    { "Red", "Green", "Blue", "White", "Dark" };
    public static final String[] types = new String[]
    { "Opaque", "Overlay", "Core" };
    
    public BlockOres(int blockOresID)
    {
        super(blockOresID, Material.rock);
        setHardness(3F).setResistance(2F).setCreativeTab(XyTech.creativeTab).setUnlocalizedName("blockOres");
    }
    
    @Override
    public Icon getIcon(int side, int meta)
    {
        return textures[meta];
    }
    
    @Override
    public int quantityDropped(Random r)
    {
        return 3 + r.nextInt(2);
    }
    
    @Override
    public int idDropped(int i, Random r, int j)
    {
        return Items.itemXychorium.itemID;
    }
    
    @Override
    public int damageDropped(int meta)
    {
        return meta;
    }
    
    @Override
    public void registerIcons(IconRegister iconReg)
    {
        for (int i = 0; i < types.length; i++)
            for (int j = 0; j < names.length; j++)
                textures[(i * 5) + j] = iconReg.registerIcon("xytech:ore/oreXychorium" + names[j] + types[i]);
    }
    
    @Override
    public void getSubBlocks(int blockID, CreativeTabs tab, List list)
    {
        for (int i = 0; i < names.length; i++)
            list.add(new ItemStack(blockID, 1, i));
    }
    
    @Override
    public int getRenderType()
    {
        return MultiLayerRenderer.renderID;
    }
    
    @Override
    public Icon getWorldTextureForPass(IBlockAccess world, int x, int y, int z, int side, int pass)
    {
        int meta = world.getBlockMetadata(x, y, z);
        
        if (pass == 0) return BlockOres.textures[5 + meta];
        else return BlockOres.textures[10 + meta];
    }
}
