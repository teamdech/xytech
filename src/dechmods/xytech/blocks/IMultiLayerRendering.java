package dechmods.xytech.blocks;

import net.minecraft.util.Icon;
import net.minecraft.world.IBlockAccess;

public interface IMultiLayerRendering
{
    public Icon getWorldTextureForPass(IBlockAccess world, int x, int y, int z, int side, int pass);
}
