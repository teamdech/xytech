package dechmods.xytech.util;

import net.minecraftforge.common.Configuration;

public class Config {
    public static int blockOresID, blockStorageID, blockTilesID, blockBricksID;
    public static int itemXychoriumID, itemXychoriditeID;

    public static void loadConfig(Configuration config) {
        blockOresID = config.get("IDs", "blockOresID", 2400).getInt(2400);
        blockStorageID = config.get("IDs", "blockStorageID", 2401).getInt(2401);
        blockTilesID = config.get("IDs", "blockTilesID", 2402).getInt(2402);
        blockBricksID = config.get("IDs", "blockBricksID", 2403).getInt(2403);

        itemXychoriumID = config.get("IDs", "itemXychoriumID", 24000).getInt(24000);
        itemXychoriditeID = config.get("IDs", "itemXychoriditeID", 24001).getInt(24001);
    }
}
