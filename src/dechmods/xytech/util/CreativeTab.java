package dechmods.xytech.util;

import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;

public class CreativeTab extends CreativeTabs {
    
    public CreativeTab(String label) {
        super(label);
    }

    @Override
    public ItemStack getIconItemStack()
    {
        return new ItemStack(1, 0, 1);
    }
}
