package dechmods.xytech.util;

import cpw.mods.fml.common.registry.LanguageRegistry;

public class Localization
{
    public static LanguageRegistry lr;
    
    public static void addNames()
    {
        lr = LanguageRegistry.instance();
        
        lr.addStringLocalization("itemGroup.xytech", "XYTech");
        
        lr.addStringLocalization("tile.blockOres.0.name", "Red Xychorium Ore");
        lr.addStringLocalization("tile.blockOres.1.name", "Green Xychorium Ore");
        lr.addStringLocalization("tile.blockOres.2.name", "Blue Xychorium Ore");
        lr.addStringLocalization("tile.blockOres.3.name", "White Xychorium Ore");
        lr.addStringLocalization("tile.blockOres.4.name", "Dark Xychorium Ore");
        
        lr.addStringLocalization("item.itemXychorium.0.name", "Red Xychorium");
        lr.addStringLocalization("item.itemXychorium.1.name", "Green Xychorium");
        lr.addStringLocalization("item.itemXychorium.2.name", "Blue Xychorium");
        lr.addStringLocalization("item.itemXychorium.3.name", "White Xychorium");
        lr.addStringLocalization("item.itemXychorium.4.name", "Dark Xychorium");
        
        lr.addStringLocalization("item.itemXychoridite.0.name", "Red Xychoridite Ingot");
        lr.addStringLocalization("item.itemXychoridite.1.name", "Green Xychoridite Ingot");
        lr.addStringLocalization("item.itemXychoridite.2.name", "Blue Xychoridite Ingot");
        lr.addStringLocalization("item.itemXychoridite.3.name", "White Xychoridite Ingot");
        lr.addStringLocalization("item.itemXychoridite.4.name", "Dark Xychoridite Ingot");
        
        lr.addStringLocalization("tile.blockXyStorage.0.name", "Red Xychorium Block");
        lr.addStringLocalization("tile.blockXyStorage.1.name", "Green Xychorium Block");
        lr.addStringLocalization("tile.blockXyStorage.2.name", "Blue Xychorium Block");
        lr.addStringLocalization("tile.blockXyStorage.3.name", "White Xychorium Block");
        lr.addStringLocalization("tile.blockXyStorage.4.name", "Dark Xychorium Block");
        
        lr.addStringLocalization("tile.blockXyTiles.0.name", "Red Xychoridite Tiles");
        lr.addStringLocalization("tile.blockXyTiles.1.name", "Green Xychoridite Tiles");
        lr.addStringLocalization("tile.blockXyTiles.2.name", "Blue Xychoridite Tiles");
        lr.addStringLocalization("tile.blockXyTiles.3.name", "White Xychoridite Tiles");
        lr.addStringLocalization("tile.blockXyTiles.4.name", "Dark Xychoridite Tiles");
        
        lr.addStringLocalization("tile.blockXyBricks.0.name", "Red Xychoridite Bricks");
        lr.addStringLocalization("tile.blockXyBricks.1.name", "Green Xychoridite Bricks");
        lr.addStringLocalization("tile.blockXyBricks.2.name", "Blue Xychoridite Bricks");
        lr.addStringLocalization("tile.blockXyBricks.3.name", "White Xychoridite Bricks");
        lr.addStringLocalization("tile.blockXyBricks.4.name", "Dark Xychoridite Bricks");
    }
}
