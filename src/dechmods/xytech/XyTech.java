package dechmods.xytech;

import net.minecraft.creativetab.CreativeTabs;
import net.minecraftforge.common.Configuration;
import cpw.mods.fml.client.registry.RenderingRegistry;
import cpw.mods.fml.common.FMLCommonHandler;
import cpw.mods.fml.common.Mod;
import cpw.mods.fml.common.Mod.EventHandler;
import cpw.mods.fml.common.Mod.Instance;
import cpw.mods.fml.common.event.FMLInitializationEvent;
import cpw.mods.fml.common.event.FMLPostInitializationEvent;
import cpw.mods.fml.common.event.FMLPreInitializationEvent;
import cpw.mods.fml.relauncher.Side;
import dechmods.xytech.blocks.Blocks;
import dechmods.xytech.items.Items;
import dechmods.xytech.render.MultiLayerRenderer;
import dechmods.xytech.util.Config;
import dechmods.xytech.util.CreativeTab;
import dechmods.xytech.util.Localization;

@Mod(modid = "xytech", name = "XyTech", version = "0.1.0-1")
public class XyTech
{
    @Instance("xytech")
    public static XyTech instanace;
    public static CreativeTabs creativeTab;
    
    public static final String[] colors = new String[]
    { "Red", "Green", "Blue", "White", "Dark" };
    
    @EventHandler
    public void preLoad(FMLPreInitializationEvent e)
    {
        Config.loadConfig(new Configuration(e.getSuggestedConfigurationFile()));
    }
    
    @EventHandler
    public void load(FMLInitializationEvent e)
    {
        creativeTab = new CreativeTab("xytech");
        Blocks.initBlocks();
        Items.initItems();
        
        if (FMLCommonHandler.instance().getSide() == Side.CLIENT) RenderingRegistry.registerBlockHandler(new MultiLayerRenderer(RenderingRegistry.getNextAvailableRenderId()));
    }
    
    @EventHandler
    public void postLoad(FMLPostInitializationEvent e)
    {
        Localization.addNames();
    }
}
