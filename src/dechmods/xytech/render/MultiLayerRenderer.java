package dechmods.xytech.render;

import org.lwjgl.opengl.GL11;

import net.minecraft.block.Block;
import net.minecraft.client.renderer.RenderBlocks;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.util.Icon;
import net.minecraft.world.IBlockAccess;
import cpw.mods.fml.client.registry.ISimpleBlockRenderingHandler;
import dechmods.xytech.blocks.Blocks;
import dechmods.xytech.blocks.IMultiLayerRendering;

public class MultiLayerRenderer implements ISimpleBlockRenderingHandler
{
    public static int renderID;
    static Tessellator t;
    
    static float minU;
    static float maxU;
    static float minV;
    static float maxV;
    
    public MultiLayerRenderer(int id)
    {
        renderID = id;
    }
    
    @Override
    public void renderInventoryBlock(Block block, int metadata, int modelID, RenderBlocks renderer)
    {
        Tessellator tessellator = Tessellator.instance;
        
        renderer.setRenderBoundsFromBlock(block);
        block.setBlockBoundsForItemRender();
        renderer.setRenderBoundsFromBlock(block);
        GL11.glRotatef(90.0F, 0.0F, 1.0F, 0.0F);
        GL11.glTranslatef(-0.5F, -0.5F, -0.5F);
        tessellator.startDrawingQuads();
        tessellator.setNormal(0.0F, -1.0F, 0.0F);
        renderer.renderFaceYNeg(block, 0.0D, 0.0D, 0.0D, renderer.getBlockIconFromSideAndMetadata(block, 0, metadata));
        tessellator.draw();
        tessellator.startDrawingQuads();
        tessellator.setNormal(0.0F, 1.0F, 0.0F);
        renderer.renderFaceYPos(block, 0.0D, 0.0D, 0.0D, renderer.getBlockIconFromSideAndMetadata(block, 1, metadata));
        tessellator.draw();
        tessellator.startDrawingQuads();
        tessellator.setNormal(0.0F, 0.0F, -1.0F);
        renderer.renderFaceZNeg(block, 0.0D, 0.0D, 0.0D, renderer.getBlockIconFromSideAndMetadata(block, 2, metadata));
        tessellator.draw();
        tessellator.startDrawingQuads();
        tessellator.setNormal(0.0F, 0.0F, 1.0F);
        renderer.renderFaceZPos(block, 0.0D, 0.0D, 0.0D, renderer.getBlockIconFromSideAndMetadata(block, 3, metadata));
        tessellator.draw();
        tessellator.startDrawingQuads();
        tessellator.setNormal(-1.0F, 0.0F, 0.0F);
        renderer.renderFaceXNeg(block, 0.0D, 0.0D, 0.0D, renderer.getBlockIconFromSideAndMetadata(block, 4, metadata));
        tessellator.draw();
        tessellator.startDrawingQuads();
        tessellator.setNormal(1.0F, 0.0F, 0.0F);
        renderer.renderFaceXPos(block, 0.0D, 0.0D, 0.0D, renderer.getBlockIconFromSideAndMetadata(block, 5, metadata));
        tessellator.draw();
        GL11.glTranslatef(0.5F, 0.5F, 0.5F);
    }
    
    @Override
    public boolean renderWorldBlock(IBlockAccess world, int x, int y, int z, Block block, int modelId, RenderBlocks renderer)
    {
        if (!(block instanceof IMultiLayerRendering)) return false;
        IMultiLayerRendering renderBlock = (IMultiLayerRendering) block;
        
        t = Tessellator.instance;
        
        int meta = world.getBlockMetadata(x, y, z);
        
        if (renderer.renderAllFaces || block.shouldSideBeRendered(world, x - 1, y, z, 4))
        {
            bindIcon(renderBlock.getWorldTextureForPass(world, x, y, z, 4, 0));
            t.setColorOpaque_F(0.6F, 0.6F, 0.6F);
            t.setBrightness(Blocks.blockOres.getMixedBrightnessForBlock(world, x - 1, y, z));
            
            t.addVertexWithUV(x, y + 1, z, minU, minV);
            t.addVertexWithUV(x, y, z, minU, maxV);
            t.addVertexWithUV(x, y, z + 1, maxU, maxV);
            t.addVertexWithUV(x, y + 1, z + 1, maxU, minV);
            
            bindIcon(renderBlock.getWorldTextureForPass(world, x, y, z, 4, 1));
            
            t.setBrightness(15728864);
            
            t.addVertexWithUV(x + 0.01F, y + 0.99F, z + 0.01F, minU, minV);
            t.addVertexWithUV(x + 0.01F, y + 0.01F, z + 0.01F, minU, maxV);
            t.addVertexWithUV(x + 0.01F, y + 0.01F, z + 0.99F, maxU, maxV);
            t.addVertexWithUV(x + 0.01F, y + 0.99F, z + 0.99F, maxU, minV);
        }
        if (renderer.renderAllFaces || block.shouldSideBeRendered(world, x, y, z + 1, 3))
        {
            bindIcon(renderBlock.getWorldTextureForPass(world, x, y, z, 3, 0));
            t.setColorOpaque_F(0.8F, 0.8F, 0.8F);
            t.setBrightness(Blocks.blockOres.getMixedBrightnessForBlock(world, x, y, z + 1));
        
            t.addVertexWithUV(x, y + 1, z + 1, minU, minV);
            t.addVertexWithUV(x, y, z + 1, minU, maxV);
            t.addVertexWithUV(x + 1, y, z + 1, maxU, maxV);
            t.addVertexWithUV(x + 1, y + 1, z + 1, maxU, minV);
            
            bindIcon(renderBlock.getWorldTextureForPass(world, x, y, z, 3, 1));
            
            t.setBrightness(15728864);
            
            t.addVertexWithUV(x + 0.01F, y + 0.99F, z + 0.99F, minU, minV);
            t.addVertexWithUV(x + 0.01F, y + 0.01F, z + 0.99F, minU, maxV);
            t.addVertexWithUV(x + 0.99F, y + 0.01F, z + 0.99F, maxU, maxV);
            t.addVertexWithUV(x + 0.99F, y + 0.99F, z + 0.99F, maxU, minV);
        }
        if (renderer.renderAllFaces || block.shouldSideBeRendered(world, x + 1, y, z, 5))
        {
            bindIcon(renderBlock.getWorldTextureForPass(world, x, y, z, 5, 0));
            t.setColorOpaque_F(0.6F, 0.6F, 0.6F);
            t.setBrightness(Blocks.blockOres.getMixedBrightnessForBlock(world, x + 1, y, z));
        
            t.addVertexWithUV(x + 1, y + 1, z + 1, minU, minV);
            t.addVertexWithUV(x + 1, y, z + 1, minU, maxV);
            t.addVertexWithUV(x + 1, y, z, maxU, maxV);
            t.addVertexWithUV(x + 1, y + 1, z, maxU, minV);
            
            bindIcon(renderBlock.getWorldTextureForPass(world, x, y, z, 5, 1));
            
            t.setBrightness(15728864);
            
            t.addVertexWithUV(x + 0.99F, y + 0.99F, z + 0.99F, minU, minV);
            t.addVertexWithUV(x + 0.99F, y + 0.01F, z + 0.99F, minU, maxV);
            t.addVertexWithUV(x + 0.99F, y + 0.01F, z + 0.01F, maxU, maxV);
            t.addVertexWithUV(x + 0.99F, y + 0.99F, z + 0.01F, maxU, minV);
        }
        if (renderer.renderAllFaces || block.shouldSideBeRendered(world, x, y, z - 1, 2))
        {
            bindIcon(renderBlock.getWorldTextureForPass(world, x, y, z, 2, 0));
            t.setColorOpaque_F(0.8F, 0.8F, 0.8F);
            t.setBrightness(Blocks.blockOres.getMixedBrightnessForBlock(world, x, y, z - 1));
        
            t.addVertexWithUV(x + 1, y + 1, z, minU, minV);
            t.addVertexWithUV(x + 1, y, z, minU, maxV);
            t.addVertexWithUV(x, y, z, maxU, maxV);
            t.addVertexWithUV(x, y + 1, z, maxU, minV);
            
            bindIcon(renderBlock.getWorldTextureForPass(world, x, y, z, 2, 1));
            
            t.setBrightness(15728864);
            
            t.addVertexWithUV(x + 0.99F, y + 0.99F, z + 0.01F, minU, minV);
            t.addVertexWithUV(x + 0.99F, y + 0.01F, z + 0.01F, minU, maxV);
            t.addVertexWithUV(x + 0.01F, y + 0.01F, z + 0.01F, maxU, maxV);
            t.addVertexWithUV(x + 0.01F, y + 0.99F, z + 0.01F, maxU, minV);
        }
        if (renderer.renderAllFaces || block.shouldSideBeRendered(world, x, y - 1, z, 0))
        {
            bindIcon(renderBlock.getWorldTextureForPass(world, x, y, z, 0, 0));
            t.setColorOpaque_F(0.5F, 0.5F, 0.5F);
            t.setBrightness(Blocks.blockOres.getMixedBrightnessForBlock(world, x, y - 1, z));
        
            t.addVertexWithUV(x, y, z, maxU, minV);
            t.addVertexWithUV(x + 1, y, z, minU, minV);
            t.addVertexWithUV(x + 1, y, z + 1, minU, maxV);
            t.addVertexWithUV(x, y, z + 1, maxU, maxV);
            
            bindIcon(renderBlock.getWorldTextureForPass(world, x, y, z, 0, 1));
            
            t.setBrightness(15728864);
            
            t.addVertexWithUV(x + 0.01F, y + 0.01F, z + 0.01F, minU, minV);
            t.addVertexWithUV(x + 0.99F, y + 0.01F, z + 0.01F, minU, maxV);
            t.addVertexWithUV(x + 0.99F, y + 0.01F, z + 0.99F, maxU, maxV);
            t.addVertexWithUV(x + 0.01F, y + 0.01F, z + 0.99F, maxU, minV);
        }
        if (renderer.renderAllFaces || block.shouldSideBeRendered(world, x, y + 1, z, 1))
        {
            bindIcon(renderBlock.getWorldTextureForPass(world, x, y, z, 1, 0));
            t.setColorOpaque_F(1, 1, 1);
            t.setBrightness(Blocks.blockOres.getMixedBrightnessForBlock(world, x, y + 1, z));
        
            t.addVertexWithUV(x, y + 1, z, minU, minV);
            t.addVertexWithUV(x, y + 1, z + 1, minU, maxV);
            t.addVertexWithUV(x + 1, y + 1, z + 1, maxU, maxV);
            t.addVertexWithUV(x + 1, y + 1, z, maxU, minV);
            
            bindIcon(renderBlock.getWorldTextureForPass(world, x, y, z, 1, 1));
            
            t.setBrightness(15728864);
            
            t.addVertexWithUV(x + 0.01F, y + 0.99F, z + 0.01F, minU, minV);
            t.addVertexWithUV(x + 0.01F, y + 0.99F, z + 0.99F, minU, maxV);
            t.addVertexWithUV(x + 0.99F, y + 0.99F, z + 0.99F, maxU, maxV);
            t.addVertexWithUV(x + 0.99F, y + 0.99F, z + 0.01F, maxU, minV);
        }
        
        return true;
    }
    
    public static void bindIcon(Icon icon)
    {
        minU = icon.getMinU();
        maxU = icon.getMaxU();
        minV = icon.getMinV();
        maxV = icon.getMaxV();
    }
    
    @Override
    public boolean shouldRender3DInInventory()
    {
        return true;
    }
    
    @Override
    public int getRenderId()
    {
        return renderID;
    }
}
